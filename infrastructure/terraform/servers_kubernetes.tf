data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "random_string" "kubernetes_admin_password" {
  length  = 16
  special = false
}

locals {
  kubernetes_admin_password = random_string.kubernetes_admin_password.result
}

resource "digitalocean_droplet" "rancherserver" {
  image     = var.kubernetes_image_do
  name      = "rancherserver"
  region    = var.kubernetes_regions_do[0]
  size      = var.kubernetes_instance_type_do
  user_data = data.template_file.userdata_server.rendered
  ssh_keys  = var.do_keypair
}

resource "aws_eip" "kubernetes_eip" {
  count = var.kubernetes_instances_node_aws
  vpc   = true
}

resource "aws_eip_association" "kubernetes_eip_association" {
  count         = var.kubernetes_instances_node_aws
  instance_id   = element(aws_instance.rancheragent_all.*.id, count.index)
  allocation_id = element(aws_eip.kubernetes_eip.*.id, count.index)
}

resource "aws_instance" "rancheragent_all" {
  count                  = var.kubernetes_instances_node_aws
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.kubernetes_instance_type_aws
  key_name               = var.aws_keypair
  user_data              = data.template_file.userdata_agent.rendered
  vpc_security_group_ids = [aws_security_group.all_servers.id]

  tags = {
    Name = "rancheragent-${count.index}-all-aws"
  }
}

resource "digitalocean_droplet" "rancheragent_all" {
  count     = var.kubernetes_instances_node_do
  image     = var.kubernetes_image_do
  name      = "rancheragent-${count.index}-all-do"
  region    = count.index < ceil(var.kubernetes_instances_node_do / 2) ? var.kubernetes_regions_do[0] : var.kubernetes_regions_do[1]
  size      = var.kubernetes_instance_type_do
  user_data = data.template_file.userdata_agent.rendered
  ssh_keys  = var.do_keypair
}

resource "digitalocean_loadbalancer" "public" {
  count                  = length(var.kubernetes_regions_do)
  name                   = "lb-${var.kubernetes_regions_do[count.index]}"
  region                 = var.kubernetes_regions_do[count.index]
  droplet_ids            = matchkeys(digitalocean_droplet.rancheragent_all.*.id, digitalocean_droplet.rancheragent_all.*.region, [var.kubernetes_regions_do[count.index]])
  redirect_http_to_https = true

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"
    certificate_id = digitalocean_certificate.ingress_cert.id
    target_port     = 80
    target_protocol = "http"
  }

  // Required for redirect_http_to_https
  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"
    target_port     = 80
    target_protocol = "http"
  }

  healthcheck {
    port                     = 80
    protocol                 = "http"
    path                     = "/healthz"
    check_interval_seconds   = 3
    response_timeout_seconds = 3
    healthy_threshold        = 2
    unhealthy_threshold      = 2
  }
}

data "template_file" "userdata_server" {
  template = file("files/userdata_server")

  vars = {
    admin_password        = local.kubernetes_admin_password
    cluster_name          = var.kubernetes_cluster_name
    docker_version_server = var.kubernetes_docker_version_server
    rancher_version       = var.kubernetes_rancher_version
  }
}

data "template_file" "userdata_agent" {
  template = file("files/userdata_agent")

  vars = {
    admin_password       = local.kubernetes_admin_password
    cluster_name         = var.kubernetes_cluster_name
    docker_version_agent = var.kubernetes_docker_version_agent
    rancher_version      = var.kubernetes_rancher_version
    server_address       = digitalocean_droplet.rancherserver.ipv4_address
  }
}

resource "cloudflare_record" "kubernetes_records_aws" {
  count   = var.kubernetes_instances_node_aws
  zone_id = var.cloudflare_zone
  name    = join(".", [aws_instance.rancheragent_all[count.index].tags["Name"], var.zone_fqdn])
  value   = aws_eip.kubernetes_eip[count.index].public_ip
  type    = "A"
  ttl     = var.dns_default_ttl
}

resource "cloudflare_record" "kubernetes_records_do" {
  count   = var.kubernetes_instances_node_do
  zone_id = var.cloudflare_zone
  name    = join(".", [digitalocean_droplet.rancheragent_all[count.index].name, var.zone_fqdn])
  value   = digitalocean_droplet.rancheragent_all[count.index].ipv4_address
  type    = "A"
  ttl     = var.dns_default_ttl
}

resource "cloudflare_record" "kubernetes_record_master" {
  zone_id = var.cloudflare_zone
  name    = join(".", ["rancher", var.zone_fqdn])
  value   = digitalocean_droplet.rancherserver.ipv4_address
  type    = "A"
  ttl     = var.dns_default_ttl
}

resource "cloudflare_record" "kubernetes_ingress_do" {
  count   = length(digitalocean_loadbalancer.public)
  zone_id = var.cloudflare_zone
  name    = var.zone_fqdn
  value   = digitalocean_loadbalancer.public[count.index].ip
  type    = "A"
  ttl     = var.dns_default_ttl
}

output "kubernetes_admin_password" {
  value = local.kubernetes_admin_password
}

locals {
  rancher_url = "https://${cloudflare_record.kubernetes_record_master.name}"
}

output "cluster_name" {
  value = var.kubernetes_cluster_name
}

output "rancher_url" {
  value = local.rancher_url
}

output "frontend_url" {
  value = "https://${var.zone_fqdn}"
}

output "zone_fqdn" {
  value = var.zone_fqdn
}