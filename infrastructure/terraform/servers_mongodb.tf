locals {
  total_instances         = var.mongodb_instances_aws + var.mongodb_instances_do
  mongodb_admin_password  = random_string.mongodb_admin_password.result
  mongodb_user_password   = random_string.mongodb_user_password.result
  mongodb_keyfile_content = base64encode(random_string.mongodb_keyfile_content.result)
}

resource "random_string" "mongodb_admin_password" {
  length  = 16
  special = false
}

resource "random_string" "mongodb_user_password" {
  length  = 16
  special = false
}

resource "random_string" "mongodb_keyfile_content" {
  length  = 600
  special = true
}

resource "aws_eip" "mongodb_eip" {
  count = var.mongodb_instances_aws
  vpc   = true
}

resource "aws_eip_association" "eip_mongodb_association" {
  count         = var.mongodb_instances_aws
  instance_id   = element(aws_instance.mongodb_instance.*.id, count.index)
  allocation_id = element(aws_eip.mongodb_eip.*.id, count.index)
}

resource "aws_instance" "mongodb_instance" {
  count                  = var.mongodb_instances_aws
  ami                    = var.mongodb_instance_ami
  instance_type          = var.mongodb_instance_type
  key_name               = var.aws_keypair
  vpc_security_group_ids = [aws_security_group.all_servers.id]

  tags = {
    Name = "mongodb-server-${count.index + 1}"
    Role = "mongodb"
  }
}

resource "digitalocean_droplet" "mongodb_instance" {
  count     = var.mongodb_instances_do
  name      = "mongodb-server-${var.mongodb_instances_aws + count.index + 1}"
  image     = "centos-7-x64"
  region    = "lon1"
  size      = "s-1vcpu-1gb"
  ssh_keys  = var.do_keypair
  user_data = <<EOF
        #cloud-config
        users:
          - name: ec2-user
            groups: sudo
            shell: /bin/bash
            sudo: ['ALL=(ALL) NOPASSWD:ALL']
            ssh-authorized-keys:
              - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDVc8KyLUKvUsM6Gu9FMwRtYga3kcNIpMKwi724yCzEc0Dd8Hm3IS97aHwh+WNHgjmWkahtTMEqMPuSkzGWHhjY3M25nydL1tzOkDrQQrthVeCrX+rPhkbS2PmfEy/a7YTNqCk6V3RqQ468vIc1G1rmwMuaQ+Xj3Q5ZMe2AQrMyZ43wLoLRM7QVwjWjUSohaQTB5Q0Az0dPndUZ/2SUQWV9jDQo1zhGlQvQW7feKesrxcl5pxH13L8ycUEoQrcltRPrZVjwv8oN4P3GXI30RWLy9ASQQqwRqldbpARv8yDhrkXt/ioqYsAypaibCjqrcVbq3qn5tD5RlByHpavIdMbhmHnwL8XMOqYnX0ThauAzD2qiBPWIT5Rb1DHwHRD5ShWpt8Ys3R+rb7vgiSsM+f52b9ITjMQ/s3e8glMw18psM5Vp9nVku51fPV5LL/B8SZ0uGg7MwthMSEQOXFKaLFKnBaTXuWx8NKGFFjKhtFQZCrauflwlox6/NQUYaaHTB0M= CSC3065-PROJ
        packages:
          - dirmngr
        manage_resolv_conf: true
        resolv_conf:
          nameservers: ['1.1.1.1']
        EOF
}

resource "cloudflare_record" "mongodb_records_aws" {
  count   = var.mongodb_instances_aws
  zone_id = var.cloudflare_zone
  name    = join(".", [aws_instance.mongodb_instance[count.index].tags["Name"], var.zone_fqdn])
  value   = aws_eip.mongodb_eip[count.index].public_ip
  type    = "A"
  ttl     = var.dns_default_ttl
}

resource "cloudflare_record" "mongodb_records_do" {
  count   = var.mongodb_instances_do
  zone_id = var.cloudflare_zone
  name    = join(".", [digitalocean_droplet.mongodb_instance[count.index].name, var.zone_fqdn])
  value   = digitalocean_droplet.mongodb_instance[count.index].ipv4_address
  type    = "A"
  ttl     = var.dns_default_ttl
}

resource "cloudflare_record" "mongodb_srv" {
  count   = local.total_instances
  zone_id = var.cloudflare_zone
  name    = "_mongodb._tcp.mongodb.csc3065"
  type    = "SRV"

  data = {
    service  = "_mongodb"
    proto    = "_tcp"
    name     = join(".", ["mongodb", var.zone_fqdn])
    priority = 0
    weight   = 0
    port     = 27017
    ttl      = var.dns_default_ttl
    target   = join(".", ["mongodb-server-${count.index + 1}", var.zone_fqdn])
  }
}

resource "null_resource" "mongodb_instance_provision" {
  depends_on = [
    aws_instance.mongodb_instance,
    digitalocean_droplet.mongodb_instance,
    cloudflare_record.mongodb_records_aws,
    cloudflare_record.mongodb_records_do,
    local_file.ansible_mongodb_hosts_file,
    cloudflare_record.mongodb_srv,
    digitalocean_firewall.all_servers,
    aws_eip_association.eip_mongodb_association
  ]

  triggers = {
    always_run = local.total_instances
  }

  connection {
    user        = "ec2-user"
    private_key = file("~/.ssh/csc3065")
    host        = null
  }

  provisioner "local-exec" {
    # Give enough time for cloud-init, DNS
    command = "sleep 65"
  }

  provisioner "ansible" {
    plays {
      playbook {
        file_path = "${path.module}/../ansible/mongodb-cluster.yml"
      }
      inventory_file = "${path.module}/../ansible/mongodb_hosts"
      extra_vars = {
        instance_count           = local.total_instances
        mongodb_admin_name       = var.mongodb_admin_name
        mongodb_admin_password   = local.mongodb_admin_password
        mongodb_user_name        = var.mongodb_user_name
        mongodb_user_password    = local.mongodb_user_password
        mongodb_keyfile_content  = local.mongodb_keyfile_content
        mongodb_replica_set_name = var.mongodb_replica_set_name
        mongodb_login_host       = cloudflare_record.mongodb_records_aws.0.name
        zone_fqdn = var.zone_fqdn
      }
    }

    ansible_ssh_settings {
      insecure_no_strict_host_key_checking = false
    }
  }
}

data "template_file" "ansible_mongodb_hosts" {
  template = file("${path.module}/files/mongodb_hosts.cfg")
  vars = {
    master   = format("%s mongodb_master=True", cloudflare_record.mongodb_records_aws.0.name)
    replicas = join("\n", slice(concat(cloudflare_record.mongodb_records_aws, cloudflare_record.mongodb_records_do), 1, local.total_instances).*.name)
  }
}

resource "local_file" "ansible_mongodb_hosts_file" {
  content  = data.template_file.ansible_mongodb_hosts.rendered
  filename = "${path.module}/../ansible/mongodb_hosts"
}

output "mongodb_srv_connection_string" {
  value       = format("mongodb+srv://${var.mongodb_admin_name}:${local.mongodb_admin_password}@mongodb.${var.zone_fqdn}/${var.mongodb_default_database_name}?ssl=false&replicaSet=${var.mongodb_replica_set_name}&authSource=admin")
  description = "The MongoDB URI string to use when connecting clients."
}

output "mongodb_user_password" {
  value = local.mongodb_user_password
}

output "mongodb_admin_password" {
  value = local.mongodb_admin_password
}