resource "tls_private_key" "private_key" {
  algorithm = "RSA"
}

resource "acme_registration" "reg" {
  account_key_pem = tls_private_key.private_key.private_key_pem
  email_address   = var.admin_email
}


resource "acme_certificate" "certificate" {
  account_key_pem = acme_registration.reg.account_key_pem
  common_name     = var.zone_fqdn

  dns_challenge {
    provider = "cloudflare"

    config = {
      CF_DNS_API_TOKEN = var.cloudflare_token
    }
  }
}

locals {
  certificate_parts          = split("/", acme_certificate.certificate.id)
  certificate_short_id_parts = split("-", element(local.certificate_parts, length(local.certificate_parts) - 1))
  certificate_short_id       = element(local.certificate_short_id_parts, length(local.certificate_short_id_parts) - 1)
}

resource "digitalocean_certificate" "ingress_cert" {
  type             = "custom"
  name             = "ingress-cert-${local.certificate_short_id}"
  private_key      = acme_certificate.certificate.private_key_pem
  leaf_certificate = acme_certificate.certificate.certificate_pem

  lifecycle {
    create_before_destroy = true
  }
}