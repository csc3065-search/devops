#######################################
# GENERIC VARIABLES                   #
#######################################
variable "aws_keypair" {
  description = "Keypair to use for AWS instances."
}

variable "do_keypair" {
  description = "Keypair to use for DO instances."
}

variable "cloudflare_email" {
  description = "Email for CloudFlare API access (used for creating DNS records)."
}

variable "cloudflare_token" {
  description = "Token for CloudFlare API access."
}

variable "cloudflare_zone" {
  description = "Zone to create DNS records in."
}

variable "digitalocean_token" {
  description = "DigitalOcean API token."
}

variable "zone_fqdn" {
  description = "FQDN to under which subdomains will be created."
}

variable "dns_default_ttl" {
  description = "Default TTL for DNS records."
}

variable "admin_ip_addr" {
  description = "Truested IP address for administration. Added to firewall configs."
}

variable "ssh_priv_key_file" {
  description = "Location of SSH private key file."
}

variable "admin_email" {
  description = "Admin email address."
}

variable "aws_profile_name" {
  description = "Name of AWS credentials profile to use."
}

variable "aws_region" {
  description = "Default AWS region."
}

#######################################
# MONGODB VARIABLES                   #
#######################################
variable "mongodb_instances_aws" {
  description = "Number of MongoDB replica set members in AWS. First member is master."
}

variable "mongodb_instances_do" {
  description = "Number of MongoDB replicat set members in DO."
}

variable "mongodb_instance_ami" {
  description = "AMI ID for MongoDB replica set members in AWS."
}

variable "mongodb_instance_type" {
  description = "Instance type for MongoDB replica set members in AWS."
}

variable "mongodb_instance_user" {
  description = "MongoDB instance username for Ansible."
}

variable "mongodb_replica_set_name" {
  description = "MongoDB replica set name."
}

variable "mongodb_user_name" {
  description = "MongoDB user login name."
}

variable "mongodb_admin_name" {
  description = "MongoDB admin login name."
}

variable "mongodb_default_database_name" {
  description = "Default database name used when providing SRV record."
}

#######################################
# KUBERNETES VARIABLES                #
#######################################
variable "kubernetes_rancher_version" {
  description = "Rancher version to use for cluster using Rancher Quickstart."
}

variable "kubernetes_instances_node_aws" {
  description = "Number of Kubernetes nodes in AWS."
}

variable "kubernetes_instances_node_do" {
  description = "Number of Kubernetes nodes in DO."
}

variable "kubernetes_regions_do" {
  description = "Regions to deploy Kubernetes nodes in to in DigitalOcean."
}

variable "kubernetes_instance_type_aws" {
  description = "Amazon AWS Instance Type"
}

variable "kubernetes_instance_type_do" {
  description = "Instance type to use in DigitalOcean for Kubernetes nodes."
}

variable "kubernetes_image_do" {
  description = "Image to use in DigitalOcean for Kubernetes nodes."
}

variable "kubernetes_docker_version_server" {
  description = "Version of Docker to run on Rancher server."
}

variable "kubernetes_docker_version_agent" {
  description = "Version of Docker to run on Kubernetes nodes."
}

variable "kubernetes_cluster_name" {
  description = "Name of Kubernetes cluster to create."
}
