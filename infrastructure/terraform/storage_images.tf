// This is commented out because destroying the bucket and copying images back into it each time
// is slow and ends up costing quite a bit in requests and bandwidth.

# resource "aws_s3_bucket" "image_store" {
#   bucket = "csc3065-image-store"

#   // Never destroy the images bucket because downloading images is heavy on
#   // the source sites and re-uploading wastes (expensive) bandwidth on AWS/S3.

#   lifecycle {
#     prevent_destroy = true
#   }

#   policy = <<POLICY
# {
#   "Id": "Policy1578071239876",
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Sid": "Stmt1578071238283",
#       "Action": [
#         "s3:DeleteObject",
#         "s3:GetObject",
#         "s3:ListBucket",
#         "s3:PutObject"
#       ],
#       "Effect": "Allow",
#       "Resource": [
#         "arn:aws:s3:::csc3065-image-store",
#         "arn:aws:s3:::csc3065-image-store/*"
#       ],
#       "Principal": {
#         "AWS": [
#           "arn:aws:iam::291611362219:user/csc3065-scraper"
#         ]
#       }
#     },
#     {
#       "Sid": "PublicRead",
#       "Effect": "Allow",
#       "Principal": "*",
#       "Action": [
#         "s3:GetObject"
#       ],
#       "Resource": [
#         "arn:aws:s3:::csc3065-image-store/*"
#       ]
#     }
#   ]
# }
#   POLICY
# }


# output "image_store_bucket" {
#   value       = aws_s3_bucket.image_store.bucket_regional_domain_name
#   description = "Regional URL for images bucket"
# }