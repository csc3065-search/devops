locals {
  trusted_address_lists = [
    aws_eip.mongodb_eip.*.public_ip,
    digitalocean_droplet.mongodb_instance.*.ipv4_address,
    aws_eip.kubernetes_eip.*.public_ip,
    digitalocean_droplet.rancheragent_all.*.ipv4_address,
    [digitalocean_droplet.rancherserver.ipv4_address],
    [var.admin_ip_addr]
  ]
  all_trusted_addresses = formatlist("%s/32", flatten(local.trusted_address_lists))
}

resource "aws_security_group" "all_servers" {
  name = "csc3065"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = local.all_trusted_addresses
    self        = true
    description = "Allow everything from trusted addresses."
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow port 80 from anywhere."
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow all egress."
  }
}

resource "digitalocean_firewall" "all_servers" {
  name        = "csc3065"
  droplet_ids = concat(digitalocean_droplet.mongodb_instance.*.id, digitalocean_droplet.rancheragent_all.*.id, [digitalocean_droplet.rancherserver.id])

  inbound_rule {
    protocol         = "tcp"
    port_range       = "all"
    source_addresses = local.all_trusted_addresses
  }

  inbound_rule {
    protocol         = "udp"
    port_range       = "all"
    source_addresses = local.all_trusted_addresses
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = local.all_trusted_addresses
  }

  // Apparently the load balancers are in front of the droplet firewall ¯\_(ツ)_/¯
  inbound_rule {
    protocol         = "tcp"
    port_range       = "80"
    source_addresses = ["0.0.0.0/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "all"
    destination_addresses = ["0.0.0.0/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "all"
    destination_addresses = ["0.0.0.0/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0"]
  }
}

resource "digitalocean_firewall" "rancher_server" {
  name        = "rancher"
  droplet_ids = [digitalocean_droplet.rancherserver.id]

  // Required for GitLab pipeline access to Kubernetes
  inbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    source_addresses = ["0.0.0.0/0"]
  }
}

