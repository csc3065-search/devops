terraform {
  backend "s3" {
    bucket = "tf-state-rw"
    key = "infra"
    region = "eu-west-1"
    profile = "csc3065"
  }
}
