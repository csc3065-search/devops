#!/bin/bash

TF_INFRA=infrastructure/terraform/
TF_APPLICATION=application/terraform/
SEED_DATA=seed-data/
TOP_DIR=$(pwd)

if [[ -n $1 ]] && [[ "${1#.*}" == destroy ]]; then
  DESTROY=true
else
  DESTROY=false
fi

if [ $DESTROY = false ]; then
  # Deploy or infrastructure
  cd "$TF_INFRA" || exit
  echo "Starting infrastructure deployment"
  terraform apply -auto-approve
  RANCHER_URL=$(terraform output rancher_url)
  RANCHER_ADMIN_PASSWORD=$(terraform output kubernetes_admin_password)
  CLUSTER_NAME=$(terraform output cluster_name)
  MONGODB_URI=$(terraform output mongodb_srv_connection_string)
  FRONTEND_URL=$(terraform output frontend_url)

  # Return to top-level directory
  cd "$TOP_DIR" || exit

  cd "$SEED_DATA" || exit
  # mongorestore --uri "$MONGODB_URI" --gzip

  # Return to top-level directory
  cd "$TOP_DIR" || exit

  # Deploy applications
  echo "Starting application deployment"
  cd $TF_APPLICATION || exit
  RANCHER_ADMIN_TOKEN=$(python ../glue.py "$RANCHER_URL" "$RANCHER_ADMIN_PASSWORD" "$CLUSTER_NAME")

  terraform apply -auto-approve -var "rancher_token=$RANCHER_ADMIN_TOKEN"

  # Output result
  echo -e "\e[1;32mDone!"
  echo -e "rancher_url = $RANCHER_URL"
  echo -e "rancher_admin_password = $RANCHER_ADMIN_PASSWORD"
  echo -e "rancher_admin_token = $RANCHER_ADMIN_TOKEN"
  echo -e "mongodb_uri = $MONGODB_URI"
  echo -e "frontend_url = $FRONTEND_URL"
else
  # Destroy applications
  cd "$TF_INFRA" || exit
  RANCHER_URL=$(terraform output rancher_url)
  RANCHER_ADMIN_PASSWORD=$(terraform output kubernetes_admin_password)
  CLUSTER_NAME=$(terraform output cluster_name)

  cd "$TOP_DIR" || exit
  cd "$TF_APPLICATION" || exit
  RANCHER_ADMIN_TOKEN=$(python ../glue.py "$RANCHER_URL" "$RANCHER_ADMIN_PASSWORD" "$CLUSTER_NAME")
  terraform destroy -auto-approve -var "rancher_token=$RANCHER_ADMIN_TOKEN"

  # Return to top-level directory
  cd "$TOP_DIR" || exit

  # Destroy infrastructure
  cd "$TF_INFRA" || exit
  terraform destroy -auto-approve
fi
