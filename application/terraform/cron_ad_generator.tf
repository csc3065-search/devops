resource "kubernetes_cron_job" "ad_generator" {
  metadata {
    name = "ad-generator"
    namespace = rancher2_namespace.project_namespace.name
  }

  spec {
    schedule = "0 */6 * * *"

    job_template {
      metadata {}

      spec {
        template {
          metadata {}

          spec {
            container {
              name  = "ad-generator"
              image = "registry.gitlab.com/csc3065-search/ad-generator:master"

              env {
                name = "IMAGE_BUCKET_NAME"
                value = "csc3065-image-store"
              }

              env {
                name = "MONGODB_URI"

                value_from {
                  secret_key_ref {
                    name = "mongodb-uri"
                    key  = "mongodb_uri"
                  }
                }
              }

              env {
                name = "AWS_ACCESS_KEY_ID"

                value_from {
                  secret_key_ref {
                    name = "image-bucket-aws-credentials"
                    key  = "aws_access_key_id"
                  }
                }
              }

              env {
                name = "AWS_SECRET_ACCESS_KEY"

                value_from {
                  secret_key_ref {
                    name = "image-bucket-aws-credentials"
                    key  = "aws_secret_access_key"
                  }
                }
              }

              env {
                name = "BASE_URL"
                value = data.terraform_remote_state.deployment.outputs.frontend_url
              }
            }

            image_pull_secrets {
              name = "gitlab"
            }

            restart_policy = "OnFailure"
          }
        }
      }
    }
  }
}
