resource "rancher2_project_alert_group" "alert_group" {
  name = "Deployment alert group"
  project_id = rancher2_project.csc3065.id
}

resource "rancher2_project_alert_rule" "frontend" {
  project_id = rancher2_project.csc3065.id
  name = "Less than 95% availability"
  group_id = rancher2_project_alert_group.alert_group.id

  workload_rule {
    selector =  kubernetes_deployment.frontend.spec[0].selector[0].match_labels
    available_percentage = 95
  }
}

resource "rancher2_project_alert_rule" "search_api" {
  project_id = rancher2_project.csc3065.id
  name = "Less than 95% availability"
  group_id = rancher2_project_alert_group.alert_group.id

  workload_rule {
    selector =  kubernetes_deployment.search_api.spec[0].selector[0].match_labels
    available_percentage = 95
  }
}

resource "rancher2_project_alert_rule" "image_proxy" {
  project_id = rancher2_project.csc3065.id
  name = "Less than 95% availability"
  group_id = rancher2_project_alert_group.alert_group.id

  workload_rule {
    selector =  kubernetes_deployment.image_proxy.spec[0].selector[0].match_labels
    available_percentage = 95
  }
}