data "rancher2_cluster" "cluster" {
  name = data.terraform_remote_state.deployment.outputs.cluster_name
}

data "rancher2_project" "default_project" {
  cluster_id = data.rancher2_cluster.cluster.id
  name       = "Default"
}

resource "rancher2_project" "csc3065" {
  cluster_id = data.rancher2_cluster.cluster.id
  name = "csc3065"
  enable_project_monitoring = true

  provisioner "local-exec" {
    command = "curl -u ${var.rancher_token} --insecure ${data.terraform_remote_state.deployment.outputs.rancher_url}/v3/clusters/${data.rancher2_cluster.cluster.id}?action=enableMonitoring --data '${file("${path.module}/files/cluster-mon-config.json")}'"
  }
}

resource "rancher2_namespace" "project_namespace" {
  name       = "csc-proj"
  project_id = rancher2_project.csc3065.id
}

resource "null_resource" "enable_cluster_monitoring" {

}