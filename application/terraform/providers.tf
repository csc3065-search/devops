provider "rancher2" {
  api_url   = data.terraform_remote_state.deployment.outputs.rancher_url
  token_key = var.rancher_token
  insecure  = true
}

provider "kubernetes" {

}

provider "helm" {

}

provider "statuscake" {
  username = var.statuscake_username
  apikey = var.statuscake_api_key
}