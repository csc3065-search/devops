resource "kubernetes_deployment" "search_api" {
  metadata {
    name = "search-api"
    namespace = rancher2_namespace.project_namespace.name

  }

  spec {
    replicas = 4

    selector {
      match_labels = {
        app = "search-api"
      }
    }

    template {
      metadata {
        labels = {
          app = "search-api"
        }
      }

      spec {
        container {
          name  = "search-api"
          image = "registry.gitlab.com/csc3065-search/search-api:master"

          port {
            name           = "8000tcp01"
            container_port = 8000
            protocol       = "TCP"
          }

          env {
            name = "MONGODB_URI"

            value_from {
              secret_key_ref {
                name = "mongodb-uri"
                key  = "mongodb_uri"
              }
            }
          }

          env {
            name = "BASE_URL"
            value = data.terraform_remote_state.deployment.outputs.frontend_url
          }

          liveness_probe {
            http_get {
              path   = "/healthz"
              port   = "8000"
              scheme = "HTTP"
            }

            initial_delay_seconds = 5
            timeout_seconds       = 1
            period_seconds        = 1
            success_threshold     = 1
            failure_threshold     = 2
          }

          readiness_probe {
            http_get {
              path   = "/healthz"
              port   = "8000"
              scheme = "HTTP"
            }

            initial_delay_seconds = 5
            timeout_seconds       = 1
            period_seconds        = 1
            success_threshold     = 2
            failure_threshold     = 2
          }

          termination_message_path = "/dev/termination-log"
          image_pull_policy        = "Always"
          stdin                    = true
          tty                      = true
        }

        restart_policy                   = "Always"
        termination_grace_period_seconds = 30
        dns_policy                       = "ClusterFirst"

        image_pull_secrets {
          name = "gitlab"
        }
      }
    }

    strategy {
      type = "RollingUpdate"

      rolling_update {
        max_surge = "2"
      }
    }

    revision_history_limit    = 10
    progress_deadline_seconds = 20
  }
}

resource "kubernetes_service" "search_api" {
  metadata {
    name = "search-api"
    namespace = rancher2_namespace.project_namespace.name
  }

  spec {
    port {
      port = 80
      target_port = 8000
    }

    selector = {
      app = "search-api"
    }
  }
}

//resource "kubernetes_ingress" "search_api" {
//  metadata {
//    name = "search-api"
//    namespace = rancher2_namespace.project_namespace.name
//
//    annotations = {
//      "nginx.ingress.kubernetes.io/rewrite-target" = "/$1"
//      "nginx.ingress.kubernetes.io/use-regex" = "true"
//      "nginx.ingress.kubernetes.io/cors-allow-origin" = "*"
//      "nginx.ingress.kubernetes.io/enable-cors": "true"
//    }
//  }
//
//  spec {
//    rule {
//      host = data.terraform_remote_state.deployment.outputs.zone_fqdn
//
//      http {
//        path {
//          path = "/api/(.+)"
//
//          backend {
//            service_name = "search-api"
//            service_port = "8000"
//          }
//        }
//      }
//    }
//  }
//}
//
