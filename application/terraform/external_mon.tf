resource "statuscake_test" "frontend_health" {
  website_name = "csc-proj frontend"
  website_url = data.terraform_remote_state.deployment.outputs.frontend_url
  test_type = "HTTP"
  check_rate = 300
  contact_group = [var.statuscake_contact_group]
}
