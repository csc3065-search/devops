variable "gitlab_username" {
  description = "GitLab username for registry access."
}

variable "gitlab_token" {
  description = "GitLab token/password for registry access."
}

variable "image_bucket_aws_access_key_id" {
  description = "AWS access key ID for use by the scraper and ad generator."
}

variable "image_bucket_aws_secret_access_key" {
  description = "AWS secret access key for use by the scraper and ad generator."
}

variable "rancher_token" {
  description = "Token to use for Rancher API."
}

variable "alerts_email_recipient" {
  description = "Recipient for email alerts."
}

variable "alerts_email_smtp_host" {
  description = "SMTP host for email alerts."
}

variable "alerts_email_smtp_port" {
  description = "SMTP port for email alerts."
}

variable "alerts_email_smtp_password" {
  description = "SMTP password for email alerts."
}

variable "statuscake_username" {
  description = "Username for StatusCake."
}

variable "statuscake_api_key" {
  description = "API key for StatusCake"
}

variable "statuscake_contact_group" {
  description = "Contact group ID for StatusCake alerts."
}