terraform {
  backend "s3" {
    bucket  = "tf-state-rw"
    key     = "application"
    region  = "eu-west-1"
    profile = "csc3065"
  }
}

data "terraform_remote_state" "deployment" {
  backend = "s3"
  config = {
    bucket  = "tf-state-rw"
    key     = "infra"
    region  = "eu-west-1"
    profile = "csc3065"
  }
}