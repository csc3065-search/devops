resource "kubernetes_deployment" "image_proxy" {
  metadata {
    name = "image-proxy"
    namespace = rancher2_namespace.project_namespace.name

  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = "image-proxy"
      }
    }

    template {
      metadata {
        labels = {
          app = "image-proxy"
        }
      }

      spec {
        container {
          name  = "image-proxy"
          image = "registry.gitlab.com/csc3065-search/image-proxy:master"

          port {
            name           = "80tcp01"
            container_port = 80
            protocol       = "TCP"
          }

          env {
            name = "DEFAULT_BACKEND"
            value = "csc3065-image-store.s3-eu-west-1.amazonaws.com"
          }

          liveness_probe {
            http_get {
              path   = "/healthz"
              port   = "80"
              scheme = "HTTP"
            }

            initial_delay_seconds = 3
            timeout_seconds       = 1
            period_seconds        = 1
            success_threshold     = 1
            failure_threshold     = 2
          }

          readiness_probe {
            http_get {
              path   = "/healthz"
              port   = "80"
              scheme = "HTTP"
            }

            initial_delay_seconds = 3
            timeout_seconds       = 1
            period_seconds        = 1
            success_threshold     = 2
            failure_threshold     = 2
          }

          termination_message_path = "/dev/termination-log"
          image_pull_policy        = "Always"
          stdin                    = true
          tty                      = true
        }

        restart_policy                   = "Always"
        termination_grace_period_seconds = 30
        dns_policy                       = "ClusterFirst"

        image_pull_secrets {
          name = "gitlab"
        }
      }
    }

    strategy {
      type = "RollingUpdate"

      rolling_update {
        max_surge = "2"
      }
    }

    revision_history_limit    = 10
    progress_deadline_seconds = 20
  }
}

resource "kubernetes_service" "image_proxy" {
  metadata {
    name = "image-proxy"
    namespace = rancher2_namespace.project_namespace.name
  }

  spec {
    port {
      port = 80
    }

    selector = {
      app = "image-proxy"
    }
  }
}

//resource "kubernetes_ingress" "image_proxy" {
//  metadata {
//    name = "image-proxy"
//    namespace = rancher2_namespace.project_namespace.name
//
//    annotations = {
//      "nginx.ingress.kubernetes.io/rewrite-target" = "/$1"
//      "nginx.ingress.kubernetes.io/use-regex" = "true"
//    }
//  }
//
//  spec {
//    rule {
//      host = data.terraform_remote_state.deployment.outputs.zone_fqdn
//
//      http {
//        path {
//          path = "/img/(.+)"
//
//          backend {
//            service_name = "image-proxy"
//            service_port = "80"
//          }
//        }
//      }
//    }
//  }
//}
