resource "rancher2_notifier" "admin_email" {
  name = "admin_email"
  cluster_id = data.rancher2_cluster.cluster.id

  smtp_config {
    default_recipient = var.alerts_email_recipient
    host = var.alerts_email_smtp_host
    port = var.alerts_email_smtp_port
    sender = var.alerts_email_recipient
    password = var.alerts_email_smtp_password
    username = var.alerts_email_recipient
  }
}