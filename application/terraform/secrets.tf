resource "rancher2_secret" "mongodb_uri" {
  name        = "mongodb-uri"
  description = "MongoDB URI for scraper and search API."
  project_id  = rancher2_project.csc3065.id

  data = {
    mongodb_uri = base64encode(data.terraform_remote_state.deployment.outputs.mongodb_srv_connection_string)
  }
}

resource "rancher2_secret" "image_bucket_aws_credentials" {
  name = "image-bucket-aws-credentials"
  description = "AWS credentials for scraper and ad generator."
  project_id = rancher2_project.csc3065.id

  data = {
    aws_access_key_id = base64encode(var.image_bucket_aws_access_key_id)
    aws_secret_access_key = base64encode(var.image_bucket_aws_secret_access_key)
  }
}