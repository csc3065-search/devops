resource "kubernetes_deployment" "frontend" {
  metadata {
    name = "frontend"
    namespace = rancher2_namespace.project_namespace.name

  }

  spec {
    replicas = 4

    selector {
      match_labels = {
        app = "frontend"
      }
    }

    template {
      metadata {
        labels = {
          app = "frontend"
        }
      }

      spec {
        container {
          name  = "frontend"
          image = "registry.gitlab.com/csc3065-search/frontend:master"

          port {
            name           = "80tcp01"
            container_port = 80
            protocol       = "TCP"
          }

          env {
            name = "BASE_URL"
            value = data.terraform_remote_state.deployment.outputs.frontend_url
          }

          liveness_probe {
            http_get {
              path   = "/healthz"
              port   = "80"
              scheme = "HTTP"
            }

            initial_delay_seconds = 5
            timeout_seconds       = 1
            period_seconds        = 1
            success_threshold     = 1
            failure_threshold     = 2
          }

          readiness_probe {
            http_get {
              path   = "/healthz"
              port   = "80"
              scheme = "HTTP"
            }

            initial_delay_seconds = 5
            timeout_seconds       = 1
            period_seconds        = 1
            success_threshold     = 2
            failure_threshold     = 2
          }

          termination_message_path = "/dev/termination-log"
          image_pull_policy        = "Always"
          stdin                    = true
          tty                      = true
        }

        restart_policy                   = "Always"
        termination_grace_period_seconds = 30
        dns_policy                       = "ClusterFirst"

        image_pull_secrets {
          name = "gitlab"
        }
      }
    }

    strategy {
      type = "RollingUpdate"

      rolling_update {
        max_surge = "2"
      }
    }

    revision_history_limit    = 10
    progress_deadline_seconds = 20
  }
}

resource "kubernetes_service" "frontend" {
  metadata {
    name = "frontend"
    namespace = rancher2_namespace.project_namespace.name
  }

  spec {
    port {
      port = 80
    }

    selector = {
      app = "frontend"
    }
  }
}

resource "kubernetes_ingress" "frontend" {
  metadata {
    name = "frontend"
    namespace = rancher2_namespace.project_namespace.name

    annotations = {
      "nginx.ingress.kubernetes.io/rewrite-target" = "/"
      "nginx.ingress.kubernetes.io/cors-allow-origin" = "*"
      "nginx.ingress.kubernetes.io/enable-cors": "true"
    }
  }

  spec {
    rule {
      host = data.terraform_remote_state.deployment.outputs.zone_fqdn

      http {
        path {
          path = "/"

          backend {
            service_name = "frontend"
            service_port = "80"
          }
        }
      }
    }
  }
}
