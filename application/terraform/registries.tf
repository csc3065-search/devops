resource "rancher2_registry" "gitlab" {
  name       = "gitlab"
  project_id = rancher2_project.csc3065.id

  registries {
    address  = "registry.gitlab.com"
    username = var.gitlab_username
    password = var.gitlab_token
  }
}
