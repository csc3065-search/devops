#!/usr/bin/python3

"""
This is a hack to get a Rancher API token after the infra Terraform config is ran and after
Rancher has been bootstrapped, but before running the application Terraform config, using the
plaintext admin password.
"""
import base64
import json
import os
import shutil
import ssl
import sys
import time

from urllib.request import Request, urlopen

import boto3

KUBECONF_LOCATION = os.path.expanduser('~/.kube/config')

RANCHER_URL = sys.argv[1]
RANCHER_ADMIN_PASSWORD = sys.argv[2]
CLUSTER_NAME = sys.argv[3]


def get_token() -> str:
    data = {'description': 'terraform-access', 'password': RANCHER_ADMIN_PASSWORD, 'ttl': 15, 'username': 'admin'}
    r = Request(RANCHER_URL + '/v3-public/localProviders/local?action=login', data=json.dumps(data).encode('utf8'))
    resp = urlopen(r, context=ssl._create_unverified_context()).read().decode()
    resp_json = json.loads(resp)
    return resp_json['token']


def get_cluster_id(token: str) -> str:
    r = Request(RANCHER_URL + "/v3/clusters?name=" + CLUSTER_NAME)
    r.add_header('Authorization', 'Basic ' + base64.b64encode(token.encode('ascii')).decode('ascii'))
    resp = urlopen(r, context=ssl._create_unverified_context()).read().decode()
    resp_json = json.loads(resp)
    return resp_json['data'][0]['id']


def get_kubeconf(token: str, cluster_id: str):
    data = {'action': 'generateKubeconfig'}
    r = Request(RANCHER_URL + "/v3/clusters/" + cluster_id + "?action=generateKubeconfig")
    r.add_header('Authorization', 'Basic ' + base64.b64encode(token.encode('ascii')).decode('ascii'))
    resp = urlopen(r, context=ssl._create_unverified_context(), data=json.dumps(data).encode('utf8')).read().decode()
    resp_json = json.loads(resp)
    return resp_json['config']


def save_kubeconf(kubeconf: str):
    if os.path.exists(KUBECONF_LOCATION):
        shutil.move(KUBECONF_LOCATION, os.path.expanduser(f'~/.kube/config.backup.{int(time.time())}'))
    with open(KUBECONF_LOCATION, 'w') as fp:
        fp.write(kubeconf)


def upload_kubeconf(kubeconf: str, key: str):
    session = boto3.session.Session(profile_name='csc3065')
    s3 = session.client('s3')
    s3.put_object(Body=kubeconf.encode('utf-8'), Key=key, Bucket='tf-state-rw')


def main():
    token = get_token()
    cluster_id = get_cluster_id(token)
    kubeconf = get_kubeconf(token, cluster_id)
    save_kubeconf(kubeconf)
    upload_kubeconf(kubeconf, 'iK37GwvZhvZN2eLy799V')
    print(token)


if __name__ == '__main__':
    main()
