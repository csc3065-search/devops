This repository contains DevOps-related code submitted in fulfillment of a cloud computing module. 

The follow components of this repository have been forked from other works:

 - `infrastructure/ansible/roles/mongodb/`: [UnderGreen/ansible-role-mongodb](https://github.com/UnderGreen/ansible-role-mongodb)
 - `infrastructure/terraform/files/userdata_{agent,server}`: [rancher/quickstart](https://github.com/rancher/quickstart)

Code mirrored from the private EEECS GitLab instance. Everything below this section represents the project at the time of submission.

# devops

This repository contains everything needed to create the project and later destroy it down to near zero-cost. It is split into two Terraform configurations. The following resources are not controlled by the scripts in this repository by default and must be created manually:

 - AWS credentials - generate these and configure your local environment with `aws configure`. Credentials should also be generated for the scraper and ad generator tasks and stored in the applicable Terraform variables.
 - CloudFlare credentials - an API token for CloudFlare will need created and stored in the applicable variables.
 - DigitalOcean credentials - and API token for DigitalOcaen will need created and stored in the applicable variable.
 - SSH keypair - an SSH keypair will need created and uploaded to both AWS and DigitalOcean and stored in the applicable variables.
 - Terraform state bucket - an S3 bucket will need created for storing the Terraform state as the S3 remote backend is used.

## Infrastructure
Creates the following resources:

- MongoDB replica set over AWS and DO
	- DNS records for each server
	- SRV record for connecting to cluster
	- Firewall configuration
- Kubernetes+Rancher cluster over AWS and DO
	- DNS record for each server
	- DO load balancer
	- Let's Encrypt certificate
	- Firewall configuration

It can optionally create the S3 bucket used for storing images, however does not by default because tearing it down, recreating it and copying seed data back over results in non-negligible bandwidth and request costs.

The following outputs are created:

 - `kubernetes_admin_password`: generated password for accessing the Kubernetes+Rancher cluster via the Rancher API.
 - `cluster_name`: cluster name as configured by the `kubernetes_cluster_name` variable. Used in the `application` configuration later on.
 - `rancher_url`: URL for accessing the Rancher admin interface.
 - `frontend_url`: default URL used for Kubernetes ingresses and equal to the `zone_fqdn` variable with a scheme.
 - `zone_fqdn`: zone FQDN as configured by the `zone_fqdn` variable. Used in the `application` configuration later on.
 - `mongodb_srv_connection_string`: MongoDB SRV connection string including the admin username and password. Used in the `application` configuration later on and can also be used in applications such as Robo 3T.
 - `mongodb_user_password`: generated MongoDB user password for the username configured by the `mongodb_admin_username` variable.
 - `mongodb_admin_password`: generated MongoDB admin password.
 - `image_store_bucket`: regional S3 endpoint URL for images store bucket.

A `.auto.tfvars` or `terraform.tfvars` file must be provided. See the `vars.tf` file for required variables.


## Application
Deploys the application on top of the infrastructure created by the previous configuration. This is a separate configuration to allow faster updates without requiring pulling infrastructure state and because the Kubernetes provider cannot be created until the cluster is available. Creates the following resources:

 - GitLab registry credentials (used when pulling images from the registry)
 - A `csc-proj` namespace in which all workloads will be deployed.
 - The `frontend` application deployment with service and ingress for public access. Proxies external requests to internal services as required.
 - The `image-proxy` application deployment with service for internal access.
 - The `search-api` application deployment with service for internal access.
 - The `ad-generator` cronjob deployment.
 - The `at-scraper` cronjob deployment.
 - Secrets for MongoDB and S3 access.

A `glue.py` script is included that fetches a Rancher token from the admin password generated in the previous configuration, fetches the cluster ID, saves the kubectl config locally and publishes the kubectl config to S3 for use by GitLab runner.

No outputs are created.
